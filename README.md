# PF2e Tweaks by Ustin

Module with small changes to FoundryVTT PF2e system.

Each tweak can be enabled separately. 

# Minimalistic clock

Not as ambitious as dedicated clock modules. No big custom overlays, no additional stuff. 

Just showing world time, compactly. On click it opens standard clock controls. Can work independently or with Simple Calendar.

![Clock](/img/clock.png "Clock") 

<!--# Compact chat cards

Make chat cards more compact by combining DC and Result lines. Same information as before, but it takes less space and easier to see in moving chat. 

![Compact Cards Example](/img/compact-cards.png "Compact Cards") 

# Alternative roll margin text

Quite a lot of people liked this design. With this option margin will be shown as (AC+x) or (DC+x). Disabled by default. 

![Alternative roll margin](/img/alternative-dc.png "Alternative roll margin") -->

# XP award macro

Writen for PF2e XP distribution. Grab it from module compendium or copy-paste from [here](https://gitlab.com/Ustin/pf2e-tweaks/-/blob/master/macro/xp-award-pf2e.js) without installing module. 

![XP macro](/img/xp-macro.png "XP macro")

# Installation

Search for this module in Foundry or use this link:

`https://gitlab.com/Ustin/PF2e-tweaks/-/raw/master/module.json`

